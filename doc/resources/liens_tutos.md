Voici les liens sur des tutoriels vidéos, à destination des candidats, sur les différentes phases de la VAE. N'hésitez pas à creuser le sujet en cherchant des ressources sur internet !
* Phase de Recevabilité : https://www.youtube.com/watch?v=QjC6VsRn3jM
* Le dossier d'expériences (la phase qui nous concerne le plus dans ce projet) : https://www.youtube.com/watch?v=49sCjY5gJts&t=
* L'entretien avec le jury: https://www.youtube.com/watch?v=HO1mibIqroM
