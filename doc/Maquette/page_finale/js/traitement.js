var page1 = document.getElementById("page1");
var page2 = document.getElementById("page2");
var bouton_envoyer = document.getElementById("envoie");
var bouton_retour = document.getElementById("retour_page");
var indicateurs = document.getElementById("indics")
var graph = document.getElementById("chart");
page2.style.display = "none";

//fontion pour cacher ou afficher une page selon le bouton appuyer (envoyer ou retour)
function cacher(){
	if (page1.style.display != "none") {
		page1.style.display = "none";
		page2.style.display = "block";
	}
	else {
    	page1.style.display = "block";
		page2.style.display = "none";
  	}
}


//ajout de deux évènements au bouton envoyer de la page 1, un pour cacher la page et l'autre pour faire la requête 
bouton_envoyer.addEventListener('click',cacher);
bouton_envoyer.addEventListener('click',function(){
	var xhr = new XMLHttpRequest();
	//simule le fait d'envoyer des données à une API
    //utilisation d'un get puisqu'il n'y a pas de données à envoyer    
    xhr.open("GET", "http://localhost/2rives/apiresultat.json");
    xhr.send(null);
    //évènement pour vérifier que la requête avec l'API s'est bien passé 
    //et qui est chargé de transmettre les données 
    xhr.addEventListener('readystatechange', function() { // On gère ici une requête asynchrone
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) { // Si le fichier est chargé sans erreur

            var response = JSON.parse(xhr.responseText)
            document.getElementById('response').innerHTML = '<div class="analyse">' + response["je_nous"]["html"] + '</div>'; // Et on affiche !
            var chart = new Chart(graph, {
            //Le type de graphs que nous voulons, ici un camembert 
            type: 'pie',
            //Les données pour notre graph
            data: {
                labels: ['je','nous'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: ['#2BF10A','#FF1F1F'],
                    data: [parseFloat((response["je_nous"]["value_indicator"])).toFixed(2),(100-parseFloat(response["je_nous"]["value_indicator"])).toFixed(2)]
                }]
            },

        });

        }

    });            
});

bouton_retour.addEventListener('click',cacher);
