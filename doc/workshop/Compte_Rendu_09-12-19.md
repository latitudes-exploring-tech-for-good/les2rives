Compte Rendu 09/12/2019

Pour la partie Word : 

    • Essayer d'afficher du texte sur une page web au lieu d'afficher un document Word
    • Essayer de traiter le texte affiché (souligner...)



Pour la partie Design : 

    • Page 1 : 
        - Simplifier le design
        - Supression des images

    • Page 2 : 
        - Suprimer le menu
        - Suprimer le graphe camembert 

    • Remplacer Indicateurs A/B/C/D par un seul ou deux indicateurs (Je/Nous)
    • Remplacer les cases à cocher par une suite d'étapes (un seul indicateur mis en valeur à chaque étape)
    • S'occuper uniquement du HTML/CSS/Javascript (API plus tard)
