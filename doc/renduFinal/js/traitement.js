//instanciation des variables nécessaire pour la suite 
var page1 = document.getElementById("page1");
var page2 = document.getElementById("page2");
var fileInput  = document.getElementById("file");
page2.style.display = "none";


$(document).ready(function() {
    $("#checkall").click(function() {  // au clic du checkbox "Tout sélectionner"
        $("input:checkbox").each(function(){   // pour chaque "checkbox" on applique la fonction
            var checked = $("#checkall").attr("checked"); //variable checked enregistre attribut "checked"
            if(checked == "checked") {  
                $(this).attr('checked', true); // Vrai : on coche tous les checkbox 
            } else {
                $(this).attr('checked', false); // Faux : on déoche tous les checkbox
            }   
        });
    });
});


//fontion pour cacher ou afficher une page selon le bouton appuyer (envoyer ou retour)
function cacher(){
    // si la page 1 est affichée alors on la cache
	if (page1.style.display != "none") {
		page1.style.display = "none";
		page2.style.display = "block";
	}
}


//fonction pour récupérer les indicateurs sélectionnés à la première page 
//et créer les balise à afficher sur la page 2
function select_indics(){
    var indicateurs = []; //création d'une liste vide qui va contenir les indicateurs séletionnés
    var inputs = document.querySelectorAll('.indicateurs_selection');//on récupère tous les indicateurs
    var inputsLength = inputs.length;
    for (var i=0;i<inputsLength;i++){// on parcourt la liste de tous les indicateurs de la page 1
        if(inputs[i].checked){ //si l'indicateur est sélectionné on effectue la tâche suivante
            indicateurs.push('<input type="radio" name="choisi" class="indicateurs_selectionne" '+ ' id='+inputs[i].id+' value='+inputs[i].value+'><label>'+inputs[i].id+'</label><br/>'); //on ajoute à liste vide avec un format utile pour la suite
            //choix du boutton radio pour ne pouvoir en sélectionner qu'un à la fois 
        }
    }
    return indicateurs;
}


//fonction pour supprimer un graphique et en rajouter un autre 
function resetCanvas(){
    $('#chart').remove(); //retire le canvas existant 
    $('#chart_container').append('<canvas id="chart" width="10" height="10"></canvas>');//rajoute un nouveau canvas qui va pouvoir contenir le nouveau graphique
}

//évènement créé quand on va cliquer sur le bouton envoyé 
document.getElementById("envoie").addEventListener('click',function(){

	//affiche sur la page 2 les indicateurs sélectionnés à la page 1 grâce à la fonction select_indics()
    //et vérifie qu'il y'a bien un fichier et au moins un indicateur sélectionné avant d'aller à la page 2
    var indicateurs = select_indics(); 
    var inputsLength = indicateurs.length;
    if (fileInput.files.length==0 && inputsLength==0) { //dans le cas où aucun fichier n'est importé et aucun indicateurs n'est sélectionné
        alert("Vous devez importer un fichier et sélectionner des indicateurs");
        return;
    }
    if (inputsLength==0) { //dans le cas où aucun indicateur n'est sélectionné
    	alert("Vous n'avez pas choisi d'indicateurs");
    	return;
    }
    if (fileInput.files.length==0) { //dans le cas où aucun fichier n'est importé
        alert("Vous n'avez pas choisi de fichier à traiter");
        return;
    }
    //boucle nécessaire pour enlever les virgules dans la liste indicateurs de la fonction select_indics()
    var indicateur = ""
    for (var i=0;i<inputsLength;i++){
    	indicateur += indicateurs[i];
    };
    document.getElementById('indics').innerHTML = indicateur;

    //récupération des données à transmettre
    var data = new FormData(); //on utilise un FormData, plus facile à envoyer à une API
    data.append("file", fileInput.files[0]); //on ajoute au formdata le fichier importé 
    var inputs = document.querySelectorAll('.indicateurs_selectionne'); //on prend tous les indicateurs sélectionnés pour pouvoir les transmettre à l'API
    var inputsLength = inputs.length;
    for (var i=0;i<inputsLength;i++){
    	data.append(inputs[i].value,inputs[i].value); //ajout au formdata les indicateurs sélectionnés
    };

    //création d'une requête POST pour envoyer les données à l'API
    var xhr = new XMLHttpRequest();    
    xhr.open("POST", "https://l2r-analyse-redac.herokuapp.com/test/");
    xhr.send(data);

    //évènement pour vérifier que la requête avec l'API s'est bien passé 
    //et qui est chargé de transmettre les données et de les afficher 
    xhr.addEventListener('readystatechange', function() { // On gère ici une requête asynchrone
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) { // Si le fichier est chargé sans erreur
           
            var response = JSON.parse(xhr.responseText); //variable qui contient la réponse de l'API au format JSON
            console.log(response);
            
            var message = document.getElementById('message');
            var valeurReference = document.getElementById('valeurIndicatif');
            
            document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>'; //On affiche !
            message.innerHTML = '<p align="center">Choisissez un indicateurs</p>'; //Un message pour indiquer qu'il faut sélectionner un indicateur pour le visualiser
            
            // Traitements lorsque l'on clique sur un bouton
			$("input[type=radio][name=choisi]").change(function() {  				
                if($(this).val() == 'jens') {

  					message.innerHTML = '';
    				document.getElementById('response').innerHTML = '<div class="analyse">' + response["je_nous"]["html"] + '</div>'; //On affiche !
    				resetCanvas();
                    var graph = document.getElementById("chart");
            		chart = new Chart(graph, {
                		//Le type de graphs que nous voulons, ici un camembert 
                		type: 'pie',
                		//Les données pour notre graph
                		data: {
                    		labels: ['je','nous'],
                    		datasets: [{
                        		backgroundColor: ['#2BF10A','#FF1F1F'],
                        		data: [parseFloat((response["je_nous"]["value_indicator"])).toFixed(2),(100-parseFloat(response["je_nous"]["value_indicator"])).toFixed(2)]
                   		}]
                		},

            		});
            		valeurReference.innerHTML = '<p>La valeur de référence est : '+response["je_nous"]["value_indicator_ref"]+'%</p>';
  				
                }if($(this).val() == 'subjectivity') {
  					message.innerHTML = '';
   					document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>'; //On affiche !
            		resetCanvas();
                    var graph = document.getElementById("chart");
            		chart = new Chart(graph, {
                		//Le type de graphs que nous voulons, ici un camembert 
                		type: 'pie',
                		//Les données pour notre graph
                		data: {
                    		labels: ['objectivité', 'subjectivité'],
                    		datasets: [{
                        		backgroundColor: ['#2BF10A','#FF1F1F'],
                        		data: [parseFloat((response["subjectivity"]["value_indicator_obj"])).toFixed(2),(parseFloat(response["subjectivity"]["value_indicator_subj"])).toFixed(2)]
                   		}]
                		},
            		})
                    valeurReference.innerHTML = "<p>La valeur de référence pour la subjectivité est : "+response["subjectivity"]["value_indicator_subj_ref"]+"% <br/> Et celle pour l'objectivité est : "+response["subjectivity"]["value_indicator_obj_ref"]+"%</p>";                  
  				
                }if($(this).val() == 'summary') {
  					resetCanvas();
                    var graph = document.getElementById("chart");
  					if(response['summary']['exists']){
  						message.innerHTML = 'Il y a un sommaire';
  					}else{
  						message.innerHTML = "Il n'y a pas de sommaire";
  					}
   					document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>'; //On affiche !
                    valeurReference.innerHTML = "";
            	
                }if($(this).val() == 'freqwords') {
  					message.innerHTML = '';
   					document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>'; //On affiche !
   					a = response["freqwords"]["values"];
   					var mots = [];
   					var freq = [];
   					for (var mot in a){
   						mots.push(mot);
   						freq.push(a[mot]);
   					};  
   					resetCanvas();
                    var graph = document.getElementById("chart");
            		var chart = new Chart(graph, {
                		type: 'bar',
                		//Les données pour notre graph
                		data: {
                    		labels: mots,
                    		datasets: [{
                        		backgroundColor: ['#FF0000','#FF3300','#FFEB00','#FFFF00','#7AFF00','#08FF00','#00FFE0','#00A1FF','#4B00FF','#8100FF'],
                        		data: freq
                   		}]
                		},
            		});
                    valeurReference.innerHTML = "";
            	
                }if($(this).val() == 'diversity_verbs') {
  					message.innerHTML = '';
    				document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>'; //On affiche !
    				resetCanvas();
                    var graph = document.getElementById("chart");
            		chart = new Chart(graph, {
                		//Le type de graphs que nous voulons, ici un camembert 
                		type: 'pie',
                		//Les données pour notre graph
                		data: {
                    		labels: ['verbes communs','autres verbes'],
                    		datasets: [{
                        		backgroundColor: ['#FF1F1F','#2BF10A'],
                        		data: [parseFloat((response["diversity_verbs"]["value_indicator"])).toFixed(2),(100-parseFloat(response["diversity_verbs"]["value_indicator"])).toFixed(2)]
                   		}]
                		},
            		});
            		valeurReference.innerHTML = 'La valeur de référence est : '+response["diversity_verbs"]["value_indicator_ref"]+'%';
  				
                }if($(this).val() == 'polarity') {
  					message.innerHTML = '';
    				document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>'; //On affiche !
    				resetCanvas();
                    var graph = document.getElementById("chart");
            		chart = new Chart(graph, {
                		//Le type de graphs que nous voulons, ici un camembert 
                		type: 'pie',
                		//Les données pour notre graph
                		data: {
                    		labels: ['positivité','négativité','neutralité'],
                    		datasets: [{
                        		backgroundColor: ['#FF1F1F','#2BF10A', '#001EFF'],
                        		data: [parseFloat((response["polarity"]["value_indicator_pos"])).toFixed(2),parseFloat((response["polarity"]["value_indicator_neg"])).toFixed(2),parseFloat((response["polarity"]["value_indicator_neut"])).toFixed(2)]
                   		}]
                		},
            		});
            		valeurReference.innerHTML = "<p>La valeur de référence pour la positivité est : "+response["polarity"]["value_indicator_pos_ref"]+"% <br/>Celle pour la négativité est : "+response["polarity"]["value_indicator_neg_ref"]+"% <br/>Et celle pour la neutralité est : "+response["polarity"]["value_indicator_neut_ref"]+"%</p>";
            	
                }if($(this).val() == 'size') {
                    resetCanvas();
                    var graph = document.getElementById("chart");
                    document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>';
                    message.innerHTML = '<p>La longueur du texte est : ' +response['size']['value_indicator']+" mots" + "<br/> Longueur de référence d'un dossier VAE : " +response['size']['value_indicator_ref']+" mots";
                    valeurReference.innerHTML = "";

                }if ($(this).val() == 'illu') {
                    resetCanvas();
                    var graph = document.getElementById("chart");
                    document.getElementById('response').innerHTML = '<div class="analyse">' + response["html_neutre"]["html"] + '</div>';
                    message.innerHTML = "<p>La nombre dillustrations est : " +response['illu']['value_indicator'] + "<br/> Nombre d'illustation d'un dossier VAE : " +response['illu']['value_indicator_ref'];
                    valeurReference.innerHTML = "";
                };               
			});

        //création du fichier pdf en utilisant la bibliothèque jsPDF 
        // var doc = new jsPDF('p','pt');
        // var rows = [["diversité des verbes",(response["diversity_verbs"]["value_indicator"]).toFixed(2),(response["diversity_verbs"]["value_indicator_ref"]).toFixed(2)]];
        // var columns = ["Nom de l'indicateur","valeur de l'indicateur", "valeur de référence"];
        // autotable(columns,rows);
        // document.getElementById("btnTelecharger").addEventListener('click',function(){
        //     doc.save("resume.pdf");
        // });


        }else if (xhr.readyState === XMLHttpRequest.DONE && xhr.status != 200) { // En cas d'erreur !
        alert('Une erreur est survenue !\n\nCode :' + xhr.status + '\nTexte : ' + xhr.statusText);
    };

    });

    cacher();
});








// // Bouton Telecharger le document

// var container = document.querySelector('#container');
// var typer = container.querySelector('#response');
// var output = container.querySelector('output');

// const MIME_TYPE = 'text/plain';

// // Rockstars use event delegation!
// document.body.addEventListener('dragstart', function(e) {
// 	var a = e.target;
// 	if (a.classList.contains('dragout')) {
// 	e.dataTransfer.setData('DownloadURL', a.dataset.downloadurl);
// 	}
// }, false);

// document.body.addEventListener('dragend', function(e) {
// 	var a = e.target;
// 	if (a.classList.contains('dragout')) {
// 		cleanUp(a);
// 	}
// }, false);

// document.addEventListener('keydown', function(e) {
// 	if (e.keyCode == 27) {  // Esc
// 		document.querySelector('details').open = false;
// 	} 
// 	else if (e.shiftKey && e.keyCode == 191) { // shift + ?
// 		document.querySelector('details').open = true;
// 	}
// }, false);

// var cleanUp = function(a) {
// a.textContent = 'Document téléchargé';
// a.dataset.disabled = true;

// // Need a small delay for the revokeObjectURL to work properly.
// setTimeout(function() {
// window.URL.revokeObjectURL(a.href);
// }, 1500);
// };

// var downloadFile = function() {
// window.URL = window.webkitURL || window.URL;

// var prevLink = output.querySelector('a');
// if (prevLink) {
// 	window.URL.revokeObjectURL(prevLink.href);
// 	output.innerHTML = '';
// }

// var bb = new Blob([typer.textContent], {type: MIME_TYPE});

// var a = document.createElement('a');
// a.download = container.querySelector('input[type="text"]').value;
// a.href = window.URL.createObjectURL(bb);
// a.textContent = 'Cliquez ici';

// a.dataset.downloadurl = [MIME_TYPE, a.download, a.href].join(':');
// a.draggable = true; // Don't really need, but good practice.
// a.classList.add('dragout');

// output.appendChild(a);

// a.onclick = function(e) {
// if ('disabled' in this.dataset) {
// 	return false;
// }

// cleanUp(this);
// };
// };


// var _gaq = _gaq || [];
// _gaq.push(['_setAccount', 'UA-22014378-1']);
// _gaq.push(['_trackPageview']);

// (function() {
// var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
// ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
// var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
// })();
// 				