/* ----------------------------------------------------------------------------------------------------*/
/* ---------------------------------Fonction : "Tout (dé)sélectionner"---------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
    $("#checkall").click(function() {  // au clic du checkbox "Tout sélectionner"
        $("input:checkbox").each(function(){   // pour chaque "checkbox" on applique la fonction
            var checked = $("#checkall").attr("checked"); //variable checked enregistre attribut "checked"
            if(checked == "checked") {  
                $(this).attr('checked', true); // Vrai : on coche tous les checkbox 
            } else {
                $(this).attr('checked', false); // Faux : on déoche tous les checkbox
            }	
        });
    });
});

/* ----------------------------------------------------------------------------------------------------*/



/* ----------------------------------------------------------------------------------------------------*/
/* -------------------------------Fonction : "Scroll animé par une transition--------------------------*/
/* ----------------------------------------------------------------------------------------------------*/

$(document).ready(function(){
    // au clic sur un lien
    $('a').on('click', function(evt){
       // bloquer le comportement par défaut: on ne rechargera pas la page
       evt.preventDefault(); 
       // enregistre la valeur de l'attribut  href dans la variable target
 var target = $(this).attr('href');
       /* le sélecteur $(html, body) permet de corriger un bug sur chrome 
       et safari (webkit) */
 $('html, body')
       // on arrête toutes les animations en cours 
       .stop()
       /* on fait maintenant l'animation vers le haut (scrollTop) vers 
        notre ancre target */
       .animate({scrollTop: $(target).offset().top}, 1000 );
    });
});

/* ----------------------------------------------------------------------------------------------------*/
