# Kickoff le 07/10/2019

Développement d'un outil d'analyse de texte et obtenir des indicateurs à destination des coachs / consultants.

* Document Word de candidature à la VAE contenant une centaine de page et des images : organigramme d'enteprise, photos
* Existant : prototype en ligne, développé par Pauline avec le langage Python : choix d'un Word, obtention au bout de 30s de calcul
des résultats, sous forme d'un document Word contenant un tableau des indicateurs.
* Les coachs pourraient alerter sur les indicateurs pas pertinent
* 75 consultants actuellement. Pool de 10 consultants près à participer au développement en testant et faisant des feedbacks.
* Résultat visé :
    * une web app dédiée écran de bureau pour que les consultants soient autonomes pour soumettre un document Word d'une candidature VAE et obtenir les indicateurs.
    * le consultant attend maximum 1 min pour voir le résultat.
* Quelques exemples d'indicateurs discutés lors de ce kickoff. Pauline propose de lister plus finement les indicateurs.
    * Indicateur nombre d'image = plus il y a d'image plus c'est personnel
    * "Je" au lieu de "on" ou "nous"
    * Phrases subjectives / objectives
    * Phrases négatives / positives
    * Longueur des textes
    * Détection des mots familiers
* Base de données de 400 candidatures permettant d'entrainer le moteur d'analyse de texte.
* Calcul d'un écart type par candidature et par indicateur par rapport à la moyenne.
* Pauline propose d'avancer sur le développement du moteur et de l'API qui resteraient en Python. Elle accepterait volontier de l'aide également sur cette partie.
* Développement de l'interface web qui utilisera l'API. Pauline n'a pas de compétences ni de préférences pour le framework Javascript (Vue.js, React, Angular ?).
* Discussion sur la possibilité d'anonymiser des candidatures pour les inclure dans le repository gitlab afin de faire des tests automatiques (tests unitaire ou fonctionnels).
* Présentation de ce qu'est une API (API Rest, Webservice, JSON).

Communication

* Groupe Whatsapp mis en place pour les échanges instantanés.
* Mailing list pour les échanges asynchrones et pas urgent.
* Scribe tournant à chaque atelier afin de documenter le projet. Compte rendu posé dans ce repository gitlab au format markdown (.md) et sous la forme AAAA-MM-JJ-titre-initial-auteur.md
